
let registeredUsers =[
	"jamieJamie21",
	"45_gunther", 
	"KingOfTheWest_2003",
	"freedomFighter47",
	"kennethTheKnight",
	"theScepter07", 
	"ninjaisme00"
]

let friendList = []

function userExist(username){
	let userExist = registeredUsers.includes(username)
	return userExist
}

function register(username){

	if(userExist(username)){
		alert("Registration failed. Username already exist!")
	}else{
		alert("Thank you for registering")
		registeredUsers.push(username)
	}
}

function addFriend(username){

	if (userExist(username)){
		friendList.push(username)
		alert(`You have added ${username} as a friend!`)
	}else{
		alert(`User not found`)
	}

}

function displayFriends(){

	if(friendList.length !== 0){
		for(let i = 0; i < friendList.length; i++){
			console.log(friendList[i])
		}
	}else{
		alert(`You currently have 0 friends. Add one First`)
	}

}

function displayNumOfFriends(){

	if(friendList.length !== 0){
		alert(`You currently have ${friendList.length} friends.`)
	}else{
		alert(`You currently have 0 friends. Add one First`)
	}
}


function deleteUser(){

	if(friendList.length !== 0){
		friendList.pop()
	}else{
		alert(`You currently have 0 friends. Add one First`)
	}
}

function deleteFriend(username){

	if(friendList.length !== 0){
		let x = friendList.indexOf(username)
		friendList.splice(x,1)
	}else{
		alert(`You currently have 0 friends. Add one First`)
	}
	

}
